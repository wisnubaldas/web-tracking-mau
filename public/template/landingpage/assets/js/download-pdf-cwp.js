// bikin pdf 
// cek object json
let cekObj = function (obj) {
    if (obj === null) {
        return 'Tidak ada data'
    } else {
        return obj
    }
}
let downloadPdf = function (data) {
    console.log(data)
    let cwpDate = cekObj(data.weighing_header)
    let buildUpDate = cekObj(data.invoice_detail)
    let baggage = function (x) {
        let y = []
        for (var key in x.build_up) {
            let obj = x.build_up[key];
            y.push([{ text: moment(obj.created_at).format('YYYY-MM-DD hh:mm:s'), colSpan: 2 }])
        }
        return y
    }
    var docDefinition = {
        content: [
            {
                text: "Cargo Weighing Proof\n\n\n\n",
                style: "header",
                alignment: 'center',
                decoration: "underline"
            },
            {
                columns: [
                    {
                        width: 90,
                        text: 'CWP'
                    },
                    {
                        width: '*',
                        text: ' : ' + data.weighing_header.ProofNumber
                    },
                ]
            },
            {
                columns: [
                    {
                        width: 90,
                        text: 'Company Agent'
                    },
                    {
                        width: '*',
                        text: ' : ' + data.weighing_header.customer.CompanyName
                    },
                ]
            },
            {
                columns: [
                    {
                        width: 90,
                        text: 'Flight No / Date'
                    },
                    {
                        width: '*',
                        text: ' : ' + data.weighing_header.detail.FlightNumber + " / " + data.weighing_header.DateOfFlight
                    },
                ]
            },
            {
                columns: [
                    {
                        width: 90,
                        text: 'Destination'
                    },
                    {
                        width: '*',
                        text: ' : ' + data.weighing_header.detail.Destination
                    },
                ]
            },
            {
                columns: [
                    {
                        width: 90,
                        text: 'AWB'
                    },
                    {
                        width: '*',
                        text: ' : ' + data.weighing_header.detail.MasterAWB
                    },
                ]
            },
            {
                style: 'tableExample',
                table: {
                    body: [
                        ['AWB-H/AWB-SMU', 'PCS', 'DIMENSION', 'ACTUAL WEIGHT', 'VOLUME WEIGHT', 'CHARGEABLE WEIGHT', 'NATURE OF GOOD'],
                        [
                            data.weighing_header.detail.HostAWB,
                            data.weighing_header.detail.Pieces,
                            `${data.weighing_header.detail.LongCargo} x ${data.weighing_header.detail.WidthCargo} x ${data.weighing_header.detail.HighCargo}`,
                            data.weighing_header.detail.NettoWeight,
                            data.weighing_header.detail.VolumeCargo,
                            data.weighing_header.detail.CAW,
                            data.weighing_header.detail.KindOfNature,
                        ],
                    ]
                },
                layout: {
                    fillColor: function (rowIndex, node, columnIndex) {
                        return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
                    }
                }
            },
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true,
                alignment: 'justify',
                color: '#0d4f38'
            },
            tableExample: {
                margin: [0, 5, 0, 15],
                fontSize: 12,
                alignment: 'center',
            },
            tableHeader: {
                bold: true,
                fontSize: 13,
                color: 'black'
            }
        }

    };
    // console.log(docDefinition)
    const pdfDocGenerator = pdfMake.createPdf(docDefinition);
    pdfDocGenerator.getDataUrl((dataUrl) => {
        console.log(dataUrl)
    });
    pdfDocGenerator.open();
}