/* ----- Custom Scripts for Destiny template ----- */

jQuery(function ($) {
  "use strict";

  // get the value of the bottom of the #main element by adding the offset of that element plus its height, set it as a variable
  var mainbottom = $('#main').offset().top;

  // on scroll,
  $(window).on('scroll', function () {

    // we round here to reduce a little workload
    stop = Math.round($(window).scrollTop());
    if (stop > mainbottom) {
      $('.navbar').addClass('past-main');
      $('.navbar').addClass('effect-main')
    } else {
      $('.navbar').removeClass('past-main');
    }

  });


  // Collapse navbar on click

  $(document).on('click.nav', '.navbar-collapse.in', function (e) {
    if ($(e.target).is('a')) {
      $(this).removeClass('in').addClass('collapse');
    }
  });

  // Owl carousel init

  $(".testimonials").owlCarousel({

    slideSpeed: 200,
    items: 1,
    singleItem: true,
    autoPlay: true,
    pagination: false
  });

  /* ------ Clients Section Owl Carousel ----- */

  $(".clients").owlCarousel({
    slideSpeed: 200,
    items: 5,
    singleItem: false,
    autoPlay: true,
    pagination: false
  });

  /* ------ jQuery for Easing min -- */

  $(function () {
    $('a.page-scroll').bind('click', function (event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
      }, 1500, 'easeInOutExpo');
      event.preventDefault();
    });
  });

  /* ----- Magnific Popup ----- */

  $('.popup').magnificPopup({
    disableOn: 0,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });


  /* ----- Jarallax Init ----- */

  $('.jarallax').jarallax({
    speed: 0.7
  });

  /* ----- Jarallax Personal Homepage Init ----- */

  $('.personal-jarallax').jarallax({
    speed: 0.7
  });



  /*----- Preloader ----- */

  $(window).load(function () {
    setTimeout(function () {
      $('#loading').fadeOut('slow', function () {
      });
    }, 3000);
  });


  /* --------- Wow Init -------*/

  new WOW().init();


  /* ----- Counter Up ----- */

  $('.counter').counterUp({
    delay: 10,
    time: 1000
  });

  /* ----- Countdown ----- */

  if ($.find('#countdown')[0]) {
    $('#countdown').countDown({
      targetDate: {
        'day': 14,
        'month': 7,
        'year': 2017,
        'hour': 11,
        'min': 13,
        'sec': 0
      },
      omitWeeks: true
    });
    //enter the count down date using the format year, month, day, time: hour, min, sec
    if ($('.day_field .top').html() == "0") $('.day_field').css('display', 'none');
  }


  /*-----------------------------------
  ----------- Scroll To Top -----------
  ------------------------------------*/

  $(window).scroll(function () {
    if ($(this).scrollTop() > 1000) {
      $('#back-top').fadeIn();
    } else {
      $('#back-top').fadeOut();
    }
  });
  // scroll body to 0px on click
  $('#back-top').on('click', function () {
    $('#back-top').tooltip('hide');
    $('body,html').animate({
      scrollTop: 0
    }, 1500);
    return false;
  });

  /* ------ Animsition ----- */

  $(".animsition").animsition({
    inClass: 'fade-in',
    outClass: 'fade-out',
    inDuration: 1500,
    outDuration: 800,
    linkElement: '.animsition-link',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
    loading: true,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: ['animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay: false,
    overlayClass: 'animsition-overlay-slide',
    overlayParentElement: 'body',
    transition: function (url) { window.location.href = url; }
  });



  /*----- Subscription Form Inter national----- */
  let sts_tracking = function (data) {
    console.log(data.filter(n => n))
    let tmp = []

    data.filter(n => n).forEach((el, index) => {
      // console.log(el.status_date)
      let b
      if (index % 2 == 0) {
        //even elements are here, you can access it by box

        b = `
          <tr class="table-success">
              <th scope="row">${el.code}</th>
              <td>${el.status_date}</td>
              <td>${el.status_time}</td>
              <td>${el.description}</td>
          </tr>  
        `
      } else {
        //odd elements are here, you can access it by box
        b = `
          <tr class="table-primary">
              <th scope="row">${el.code}</th>
              <td>${el.status_date}</td>
              <td>${el.status_time}</td>
              <td>${el.description}</td>
          </tr>  
        `
      }

      tmp.push(b)
    });
    let a = `
            <div class="table-responsive">
                <table class="table table-xs table-striped">
                    <thead>
                        <tr class="bg-info">
                            <th>Code</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${tmp.join("")}
                    </tbody>
                </table>
            </div>
            `
    return a
  }
  let tmp_data = function (data) {

    return `
        <style>
        .txt-blue { color:"#09509b"; font-weight: 900; margin-left: 5px; }
        .list-unstyled > li { padding-bottom:5px; text-align: start;}
        </style>
        <div class="col-md-6 text-left">
          <ul class="list-unstyled">
            <li>Gudang : <span class="txt-blue">${data.tps}</span></li>
            <li>Tipe: <span class="txt-blue">${data.gate_type.toUpperCase()}</span></li>
            <li>SMU : <span class="txt-blue">${data.waybill_smu}</span></li>
            <li>AWB : <span class="txt-blue">${data.hawb}</span></li>
            <li>Airline : <span class="txt-blue">${data.airline_code}</span></li>
            <li>Flight : <span class="txt-blue">${data.flight_no}</span></li>
            <li>Origin : <span class="txt-blue">${data.origin}</span></li>
            <li>Destenation : <span class="txt-blue">${data.dest}</span></li>
          </ul>
        </div>
        <div class="col-md-6 text-left">
          <ul class="list-unstyled">
            <li>Good : <span class="txt-blue">${data.kindofgood}</span></li>
            <li>Koli: <span class="txt-blue">${data.koli}</span></li>
            <li>Volume: <span class="txt-blue">${data.volume}</span></li>
            <li>Netto: <span class="txt-blue">${data.netto}</span></li>
            <li>Shipper: <span class="txt-blue">${data.shipper_name}</span></li>
            <li>Consignee: <span class="txt-blue">${data.consignee_name}</span></li>
          </ul>
        </div>
    `;
  }


  let frm = document.getElementById('frm-track')

  // triger form international
  let frmInter = frm.classList.contains("form-international");
  if (frmInter) {
    $('.form-international').submit(function (e) {
      e.preventDefault();
      let postdata = $('.form-international').serialize();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        type: 'POST',
        url: '/get-international',
        data: postdata,
        dataType: 'json',
        success: function (json) {
          $('.status').html(json.status).addClass("fadeIn animated")
            .css({ "color": "#1e7f52", "font-size": "150%" })
          $('.message').html(json.message).append("<hr />").addClass("fadeIn animated")
            .css({ "font-weight": "900" })
          $('.data').html(tmp_data(json.data)).addClass("fadeIn animated")
          $('.status2').html(sts_tracking(json.data.status)).addClass("fadeIn animated")
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log(textStatus)
          console.log(errorThrown)
          $('.status').html("Data tidak ditemukan!")
            .addClass("fadeIn animated")
            .css({ "color": "#b8312c", "font-size": "200%" })
          $('.message').html('<br>')
          $('.data').html("Terjadi kesalahan pada query data yang di tuju tidak ditemukan")
            .addClass("fadeIn animated")
        }
      });
    });
  }

  // triger form domestik
  let frmDom = frm.classList.contains('form-domestik')
  if (frmDom) {


    // cek object json
    var cekObj = function (obj) {
      if (obj === null) {
        return 'Tidak ada data'
      } else {
        return obj
      }
    }
    // generate template response
    let tampilanData = function (data) {
      // console.log(data)

      return `
      <div style="padding:10px;">
      </div>
      <table class="table table-hover table-sm">
          <tr class="info">
            <td>MAWB</td>
            <td>Origin</td>
            <td>Destination</td>
          </tr>
           <tr>
            <td>${data.MasterAWB}</td>
            <td>${data.Origin}</td>
            <td>${data.Destination}</td>
          </tr>
           <tr class="info">
            <td>Airlines Code</td>
            <td colspan="2">Kind Of Good</td>
          </tr>
           <tr>
            <td>${data.AirlinesCode}</td>
            <td colspan="2">${data.kindofgood}</td>
          </tr>
          <tr class="info">
            <td>Pieces</td>
            <td>Pallet</td>
            <td>Volume</td>
          </tr>
           <tr>
            <td>${data.weighing_header.TotalPieces}</td>
            <td>${data.weighing_header.TotalPallet}</td>
            <td>${data.weighing_header.TotalVolume}</td>
          </tr>
          <tr class="info">
            <td>Gross</td>
            <td>Netto</td>
            <td>Flight Number</td>
          </tr>
           <tr>
            <td>${data.weighing_header.TotalGross}</td>
            <td>${data.weighing_header.TotalNetto}</td>
            <td>${data.weighing_header.FlightNumber}</td>
          </tr>
      </table>
      `
    }
    let tampilanStatus = function (data) {
      let cwpDate = cekObj(data.weighing_header)
      let buildUpDate = cekObj(data.invoice_detail)
      let baggage = function (x) {
        let y = []
        for (var key in x.build_up) {
          let obj = x.build_up[key];
          y.push(`<li>${moment(obj.created_at).format('YYYY-MM-DD hh:mm:s')}</li>`)
        }
        return y.join('')
      }

      return `
       <ul class="list-group">
        <li class="list-group-item">Approval <span class="badge bg-primary">${data.DateOfSLI} ${data.TimeOFSLI}</span></li>
        <li class="list-group-item">CWP <span class="badge bg-primary">${cwpDate.DateOfEntry} ${cwpDate.TimeOfEntry}</span></li>
        <li class="list-group-item">Build Up <span class="badge bg-primary">${buildUpDate.created_at}</span></li>
        <li class="list-group-item">
            <strong>Baggage</stong></br></br>
            <ul class="list-unstyled">
              ${baggage(data)}
            </ul> 
        </li>
      </ul>
      

      `
    }

    $('.form-domestik').submit(function (e) {
      e.preventDefault();
      let postdata = $('.form-domestik').serialize();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        type: 'POST',
        url: '/get-domestic',
        data: postdata,
        dataType: 'json',
        success: function (json) {
          // console.log(json)
          $('.status').html('Status Cargo').addClass("fadeIn animated")
            .css({ "color": "#1e7f52", "font-size": "150%" })

          $('.message').html(`HAWB ${json.MasterAWB}`).append("<hr />").addClass("fadeIn animated")
            .css({ "font-weight": "900" })
          // data adalah status
          $('.data').html(tampilanStatus(json)).addClass("fadeIn animated")
          // status adalah data
          $('.status2').html(tampilanData(json)).addClass("fadeIn animated")

          $('#download-pdf').click(function () {
            downloadPdf(json)
          })

          grecaptcha.reset();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          // console.log(textStatus)
          // console.log(errorThrown)
          // console.log(XMLHttpRequest.responseJSON.error)
          $('.status').html(textStatus)
            .addClass("fadeIn animated")
            .css({ "color": "#b8312c", "font-size": "200%" })
          $('.message').html('<br>')
          $('.data').html(XMLHttpRequest.responseJSON.error).addClass("fadeIn animated")
          grecaptcha.reset();


        }

      });
    })

  }


  /*----- Subscription Form Inter Deomestik----- */
  // font-weight: 900; color:#1e7f52
  function rechapchaCallback(r) {
    console.log(r)
  }
});
