<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use \Carbon\Carbon;
use Illuminate\Support\Str;

Route::get('mau-ntp/{timezone}', function ($timezone) {
    $timezone = Str::headline(Str::replace('-', '/', $timezone));
    try {
        $time = Carbon::now($timezone);
        $obj = $time->toObject();
        return [
            "utc_offset"   => $time->getOffsetString(),
            "timezone"     => $time->tzName,
            "day_of_week"  => $obj->dayOfWeek,
            "day_of_year"  => $obj->dayOfYear,
            "datetime"     => $time->toAtomString(),// "2024-08-05T19:30:54.668498+07:00",
            "utc_datetime" => $time->tz('UTC'),// "2024-08-05T12:30:54.668498+00:00",
            "unixtime"     => $time->timestamp,
            "raw_offset"   => intltz_get_raw_offset(\IntlTimeZone::createDefault()), //25200,
            "week_number"  => $time->firstWeekDay,
            "dst"          => false,
            "abbreviation" => "WIB",
            "dst_offset"   => 0,
            "dst_from"     => null,
            "dst_until"    => null,
            "client_ip"    => \Request::ip(),
        ];
    } catch (\InvalidArgumentException $exeption) {
        return response(['error' => $exeption->getMessage()], 500);
    }
});