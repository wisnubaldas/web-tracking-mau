<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TrackingController;

Route::get('/', [App\Http\Controllers\SearcingController::class, 'index']);
Route::get('/tracking', [TrackingController::class, 'index']);
Route::post('/tracking', [TrackingController::class, 'tracking']);

Route::get('/searching', [App\Http\Controllers\SearcingController::class, 'index']);
Route::post('/searching', [App\Http\Controllers\SearcingController::class, 'get_track'])->name('search');

Route::post('/get-international', [App\Http\Controllers\SearcingController::class, 'get_international']);
Route::post('/get-domestic', [App\Http\Controllers\DomesticController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware(['auth'])->group(function () {
    Route::get('dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
    Route::get('cwp', [App\Http\Controllers\CWPController::class, 'index'])->name('cwp');
    Route::get('cwp-data', [App\Http\Controllers\CWPController::class, 'cwp_data'])->name('cwp-data');
    Route::get('cwp-pdf/{pn}', [App\Http\Controllers\CWPController::class, 'cwp_pdf'])->name('cwp-pdf');
});
