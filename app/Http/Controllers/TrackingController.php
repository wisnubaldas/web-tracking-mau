<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datax['status'] = 'failed';
        $datax['data'] = null;
        $data = json_decode(json_encode($datax));
        // print "<pre>";
        // print_r($data);
        // print "</pre>";
        // die;
        return view('track', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tracking(Request $request)
    {
        $trackNo = $request->input('track');
        $url = 'https://dev-tracking.mitraadira.com/api/pencarian';
        $data = array("host" => $trackNo);
        $ch = curl_init($url);
        $data_string = json_encode($data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($result);

        // print "<pre>";
        // print_r(json_decode($result));
        // print "</pre>";
        // die;
        // $result = json_decode($result);
        // $data['status'] = 'failed';
        // $data['data'] = null;

        // if (!empty($result)) {
        //     $data['status'] = 'success';
        //     $data['data'] = json_decode($result);
        // }

        // if (count($data['data']) == 0) {
        //     $data['status'] = 'failed';
        //     $data['data'] = null;
        // }

        return view('track', compact('data'));
    }
}
