<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use \App\Models\Warehouse\OutWeighingHeader;

use Yajra\DataTables\DataTables;

class CWPController extends Controller
{
    public function index()
    {
        return view('back.cwp');
    }
    public function cwp_data(Request $request)
    {
        setlocale(LC_ALL, 'IND');
        $now = Carbon::now()->format('Y-m-d');
        $weighing = OutWeighingHeader::select('ProofNumber', 'MasterAWB', 'shipperCode', 'DateOfFlight', 'TimeOfEntry');
        return Datatables::of($weighing)
            // ->filter(function ($query) use ($request) {
            //     if ($request->has('ProofNumber')) {
            //         $query->where('ProofNumber', '=', $request->get('ProofNumber'));
            //     }
            // })
            ->addColumn('pdf', function ($w) {
                return '<a target="_blank" href="' . route('cwp-pdf', $w->ProofNumber) . '" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-edit"></i> Print PDF</a>';
            })
            ->rawColumns(['pdf'])
            ->make(true);
    }
    public function cwp_pdf($pn)
    {
        $cwp = \App\Models\Warehouse\OutWeighingDetail::
            join('out_weighingheader', 'out_weighingheader.ProofNumber', '=', 'out_weighingdetail.ProofNumber')
            ->join('mst_customer', 'out_weighingheader.shipperCode', '=', 'mst_customer.CustomerCode')
            ->where('out_weighingdetail.ProofNumber', $pn)->first();
        return view('back.cwp-pdf', compact('cwp'));
    }
}
