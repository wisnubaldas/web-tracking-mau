<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DomesticController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'g-recaptcha-response' => 'recaptcha',
            // OR since v4.0.0
            recaptchaFieldName()   => recaptchaRuleName()
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => 'Error capcha'], 500);
        }

        if (!$request->host) {
            return response()->json(['error' => 'Host Master AWB harus di isi'], 500);
        }

        $inc = $this->incoming($request);
        if ($inc) {
            return $this->inc_to_array($inc);
        }
        $out = $this->outgoing($request);
        if ($out) {
            return $this->out_to_array($out);
        }

        return response()->json(['error' => 'Data tidak di temukan'], 500);
    }
    private function out_to_array($out)
    {
        return $out;
        // $td_outbond_acceptance = $out->td_outbond_acceptance ? [
        //     "status_date" => $out->td_outbond_acceptance->status_date,
        //     "status_time" => $out->td_outbond_acceptance->status_time,
        //     "code"        => $out->td_outbond_acceptance->code,
        //     "description" => $out->td_outbond_acceptance->status
        // ] : null;
        // $td_outbond_weighing = $out->td_outbond_weighing ? [
        //     "status_date" => $out->td_outbond_weighing->status_date,
        //     "status_time" => $out->td_outbond_weighing->status_time,
        //     "code"        => $out->td_outbond_weighing->code,
        //     "description" => $out->td_outbond_weighing->status
        // ] : null;
        // $td_outbond_manifest = $out->td_outbond_manifest ? [
        //     "status_date" => $out->td_outbond_manifest->status_date,
        //     "status_time" => $out->td_outbond_manifest->status_time,
        //     "code"        => $out->td_outbond_manifest->code,
        //     "description" => $out->td_outbond_manifest->status
        // ] : null;
        // $td_outbond_storage = $out->td_outbond_storage ? [
        //     "status_date" => $out->td_outbond_storage->status_date,
        //     "status_time" => $out->td_outbond_storage->status_time,
        //     "code"        => $out->td_outbond_storage->code,
        //     "description" => $out->td_outbond_storage->status
        // ] : null;
        // $td_outbond_buildup = $out->td_outbond_buildup ? [
        //     "status_date" => $out->td_outbond_buildup->status_date,
        //     "status_time" => $out->td_outbond_buildup->status_time,
        //     "code"        => $out->td_outbond_buildup->code,
        //     "description" => $out->td_outbond_buildup->status
        // ] : null;
        // $td_outbond_delivery_staging = $out->td_outbond_delivery_staging ? [
        //     "status_date" => $out->td_outbond_delivery_staging->status_date,
        //     "status_time" => $out->td_outbond_delivery_staging->status_time,
        //     "code"        => $out->td_outbond_delivery_staging->code,
        //     "description" => $out->td_outbond_delivery_staging->status
        // ] : null;
        // $td_outbond_delivery_aircarft = $out->td_outbond_delivery_aircarft ? [
        //     "status_date" => $out->td_outbond_delivery_aircarft->status_date,
        //     "status_time" => $out->td_outbond_delivery_aircarft->status_time,
        //     "code"        => $out->td_outbond_delivery_aircarft->code,
        //     "description" => $out->td_outbond_delivery_aircarft->status
        // ] : null;
        // $td_outbond_loading_aircarft = $out->td_outbond_loading_aircarft ? [
        //     "status_date" => $out->td_outbond_loading_aircarft->status_date,
        //     "status_time" => $out->td_outbond_loading_aircarft->status_time,
        //     "code"        => $out->td_outbond_loading_aircarft->code,
        //     "description" => $out->td_outbond_loading_aircarft->status
        // ] : null;

        // $data = [
        //     'status'  => 'success',
        //     'message' => 'Tracking hawb ' . $out->waybill_smu,
        //     'data'    => [
        //         'tps'            => $out->tps,
        //         'gate_type'      => $out->gate_type,
        //         'waybill_smu'    => $out->waybill_smu,
        //         'hawb'           => $out->hawb,
        //         'koli'           => $out->koli,
        //         "netto"          => $out->netto,
        //         "volume"         => $out->volume,
        //         "kindofgood"     => $out->kindofgood,
        //         "airline_code"   => $out->airline_code,
        //         "flight_no"      => $out->flight_no,
        //         "origin"         => $out->origin,
        //         "transit"        => $out->transit,
        //         "dest"           => $out->dest,
        //         "shipper_name"   => $out->shipper_name,
        //         "consignee_name" => $out->consignee_name,
        //         "status"         => [
        //             $td_outbond_acceptance,
        //             $td_outbond_weighing,
        //             $td_outbond_manifest,
        //             $td_outbond_storage,
        //             $td_outbond_buildup,
        //             $td_outbond_delivery_staging,
        //             $td_outbond_delivery_aircarft,
        //             $td_outbond_loading_aircarft,
        //         ]
        //     ]
        // ];
        // return $data;
    }
    // 'HostAWB', 'Pieces', 'LongCargo', 'WidthCargo', 'HighCargo', 'NettoWeight', 'VolumeCargo', 'CAW', 'KindOfNature'
    private function outgoing($request)
    {
        $outb = \App\Models\Warehouse\OutApproval::with([
            'weighing_header' => function ($q) {
                return $q->with(['detail', 'customer']);
            },
            'invoice_detail'  => function ($q) {
                return $q->select(['MasterAWB', 'created_at',]);
            },
            'build_up'        => function ($q) {
                return $q->select(['MasterAWB', 'created_at']);
            },

        ]);
        $outb->where('MasterAWB', $request->host);
        return $outb->first();
    }
    private function inc_to_array($inc)
    {
        $td_inbound_delivery_aircarft = $inc->td_inbound_delivery_aircarft ? [
            "status_date" => $inc->td_inbound_delivery_aircarft->status_date,
            "status_time" => $inc->td_inbound_delivery_aircarft->status_time,
            "code"        => $inc->td_inbound_delivery_aircarft->code,
            "description" => $inc->td_inbound_delivery_aircarft->status
        ] : null;
        $td_inbound_breakdown = $inc->td_inbound_breakdown ? [
            "status_date" => $inc->td_inbound_breakdown->status_date,
            "status_time" => $inc->td_inbound_breakdown->status_time,
            "code"        => $inc->td_inbound_breakdown->code,
            "description" => $inc->td_inbound_breakdown->status
        ] : null;
        $td_inbound_storage = $inc->td_inbound_storage ? [
            "status_date" => $inc->td_inbound_storage->status_date,
            "status_time" => $inc->td_inbound_storage->status_time,
            "code"        => $inc->td_inbound_storage->code,
            "description" => $inc->td_inbound_storage->status
        ] : null;
        $td_inbound_clearance = $inc->td_inbound_clearance ? [
            "status_date" => $inc->td_inbound_clearance->status_date,
            "status_time" => $inc->td_inbound_clearance->status_time,
            "code"        => $inc->td_inbound_clearance->code,
            "description" => $inc->td_inbound_clearance->status
        ] : null;
        $td_inbound_pod = $inc->td_inbound_pod ? [
            "status_date" => $inc->td_inbound_pod->status_date,
            "status_time" => $inc->td_inbound_pod->status_time,
            "code"        => $inc->td_inbound_pod->code,
            "description" => $inc->td_inbound_pod->status
        ] : null;

        $data = [
            'status'  => 'success',
            'message' => 'Tracking hawb ' . $inc->waybill_smu,
            'data'    => [
                'tps'            => $inc->tps,
                'gate_type'      => $inc->gate_type,
                'waybill_smu'    => $inc->waybill_smu,
                'hawb'           => $inc->hawb,
                'koli'           => $inc->koli,
                "netto"          => $inc->netto,
                "volume"         => $inc->volume,
                "kindofgood"     => $inc->kindofgood,
                "airline_code"   => $inc->airline_code,
                "flight_no"      => $inc->flight_no,
                "origin"         => $inc->origin,
                "transit"        => $inc->transit,
                "dest"           => $inc->dest,
                "shipper_name"   => $inc->shipper_name,
                "consignee_name" => $inc->consignee_name,
                "status"         => [
                    $td_inbound_delivery_aircarft,
                    $td_inbound_breakdown,
                    $td_inbound_storage,
                    $td_inbound_clearance,
                    $td_inbound_pod,
                ]
            ]
        ];
        return $data;
    }

    private function incoming($request)
    {
        $inb = \App\Models\TpsOnline\ThInbound::with(
            [
                'td_inbound_delivery_aircarft' => function ($q) {
                    return $q->select(['id_header', 'status_date', 'status_time']);
                },
                'td_inbound_breakdown'         => function ($q) {
                    return $q->select(['id_header', 'status_date', 'status_time']);
                },
                'td_inbound_storage'           => function ($q) {
                    return $q->select(['id_header', 'status_date', 'status_time']);
                },
                'td_inbound_clearance'         => function ($q) {
                    return $q->select(['id_header', 'status_date', 'status_time']);
                },
                'td_inbound_pod'               => function ($q) {
                    return $q->select(['id_header', 'status_date', 'status_time']);
                }
            ]
        );
        $inb->where('waybill_smu', $request->host);
        return $inb->first();
    }
}