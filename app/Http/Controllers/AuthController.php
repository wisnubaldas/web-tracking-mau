<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\PersonalAccessToken;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {


        if ($request->ajax()) {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                $user->tokens()->delete();
                $token = $user->createToken('login-token')->plainTextToken;
                return response()->json(['token' => $token]);
            }

            return response()->json(['message' => 'Invalid credentials'], 401);
        }
        $validated = $request->validate([
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('dashboard');
        }

        return back()->with([
            'loginError' => 'email atau Password salah',
        ]);

    }
    public function renew(Request $request)
    {
        $token = PersonalAccessToken::findToken($request->bearerToken());
        $user = $token->tokenable;
        $u = Auth::user($user->id);
        dd($u);
        $user->currentAccessToken()->delete();
        $token = $user->createToken('baru')->plainTextToken;
        return response()->json([
            'token' => $token,
        ]);
    }
}