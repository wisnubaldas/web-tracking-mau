<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tracking\DeliveryOrder;
use App\Models\TpsOnline\ThInbound;
use App\Models\TpsOnline\TdInboundStorage;
use App\Models\TpsOnline\TdInboundClearance;
use App\Models\TpsOnline\TdInboundPod;


class DeliveryOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DeliveryOrder::paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $th_inbound = ThInbound::where('waybill_smu', $request->waybill_smu)->first();
        if ($th_inbound) {
            $th_inbound->kindofgood = $request->kindofgood;
            $th_inbound->shipper_name = $request->shipper_name;
            $th_inbound->save();
            $td_storage = new TdInboundStorage;
            $td_storage->id_header = $th_inbound->id_;
            $td_storage->status_date = $request->date_of_do;
            $td_storage->status_time = $request->time_of_do;
            $td_storage->save();
            $td_clearance = new TdInboundClearance;
            $td_clearance->id_header = $th_inbound->id_;
            $td_clearance->status_date = $request->date_of_do;
            $td_clearance->status_time = $request->time_of_do;
            $td_clearance->save();
            $td_pod = new TdInboundPod;
            $td_pod->id_header = $th_inbound->id_;
            $td_pod->status_date = $request->date_of_do;
            $td_pod->status_time = $request->time_of_do;
            $td_pod->save();
            return $this->ResponismeTeror(200, "Sukses Update " . $request->waybill_smu, $th_inbound->id_);
        } else {
            return $this->ResponismeTeror(500, "Error update data tidak ditemukan " . $request->waybill_smu, $request->waybill_smu);
        }

        // dd($request->all());
        // return DeliveryOrder::insert($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}