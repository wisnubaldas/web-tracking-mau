<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TpsOnline\ThInbound;
use App\Models\TpsOnline\TdInboundDeliveryAircarft;
use App\Models\TpsOnline\TdInboundBreakdown;

class InboundController extends Controller
{
    /**
     * Api ke table inbound aja
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $m = new ThInbound;
        $m->tps = $request->tps;
        $m->gate_type = $request->gate_type;
        $m->waybill_smu = $request->waybill_smu;
        $m->hawb = $request->hawb;
        $m->koli = $request->koli;
        $m->netto = $request->netto;
        $m->volume = $request->volume;
        $m->kindofgood = $request->kindofgood;
        $m->airline_code = $request->airline_code;
        $m->flight_no = $request->flight_no;
        $m->origin = $request->origin;
        $m->transit = $request->transit;
        $m->dest = $request->dest;
        $m->shipper_name = $request->shipper_name;
        $m->consignee_name = $request->consignee_name;
        $m->_is_active = $request->_is_active;
        $m->_remarks_last_update = $request->_remarks_last_update;
        $m->full_check = $request->full_check;
        $m->save();
        if ($m->id_) {
            $br = new TdInboundBreakdown;
            $br->id_header = $m->id_;
            $br->status_date = $request->DateOfBreakdown;
            $br->status_time = $request->TimeOfBreakdown;
            $br->save();
            if ($br->id_) {
                $data = new TdInboundDeliveryAircarft;
                $data->id_header = $m->id_;
                $data->status_date = $request->DateOfArrival;
                $data->status_time = $request->TimeOfArrival;
                $data->save();
                if ($data->id_) {
                    return $this->ResponismeTeror(200, "sukses insert data ThInbound {$request->waybill_smu}", $m->id_);
                }
            }
        } else {
            return $this->ResponismeTeror(404, "Ada yg gagal insert", $m);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($waybill_smu)
    {
        $m = ThInbound::where(["waybill_smu" => $waybill_smu])->first();
        if ($m) {
            return $this->ResponismeTeror(200, "waybill_smu data", $m);
        }
        return $this->ResponismeTeror(404, "waybill_smu data", $m);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}