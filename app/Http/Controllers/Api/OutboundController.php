<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TpsOnline\ThOutbound;
use App\Models\TpsOnline\TdOutbondAcceptance;
use Carbon\Carbon;

class OutboundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $my_date = Carbon::parse($request->date_of_entry . " " . $request->time_of_entry);
        $out = new ThOutbound;
        $out->gate_type = $request->gate_type;
        $out->waybill_smu = $request->waybill_smu;
        $out->hawb = $request->hawb;
        $out->koli = $request->koli;
        $out->netto = $request->netto;
        $out->volume = $request->volume;
        $out->kindofgood = $request->kindofgood;
        $out->airline_code = $request->airline_code;
        $out->flight_no = $request->flight_no;
        $out->origin = $request->origin;
        $out->transit = $request->transit;
        $out->dest = $request->dest;
        $out->shipper_name = $request->shipper_name;
        $out->consignee_name = $request->consignee_name;
        $out->save();
        if ($out->id_) {
            $ac = new TdOutbondAcceptance;
            $ac->id_header = $out->id_;
            $ac->status_date = $request->date_of_entry;
            $ac->status_time = $request->time_of_entry;
            $ac->save();
            if ($ac->id_) {
                $d = $my_date->addHour();
                $wg = new \App\Models\TpsOnline\TdOutbondWeighing;
                $wg->id_header = $out->id_;
                $wg->status_date = $d->format('Y-m-d');
                $wg->status_time = $d->format('H:i:s');
                $wg->save();
                if ($wg->id_) {
                    $d = $my_date->addHour();
                    $mn = new \App\Models\TpsOnline\TdOutbondManifest;
                    $mn->id_header = $out->id_;
                    $mn->status_date = $d->format('Y-m-d');
                    $mn->status_time = $d->format('H:i:s');
                    $mn->save();
                    if ($mn->id_) {
                        $d = $my_date->addHour();
                        $st = new \App\Models\TpsOnline\TdInboundStorage;
                        $st->id_header = $out->id_;
                        $st->status_date = $d->format('Y-m-d');
                        $st->status_time = $d->format('H:i:s');
                        $st->save();
                        if ($st->id_) {
                            $d = $my_date->addHour();
                            $bl = new \App\Models\TpsOnline\TdOutbondBuildup;
                            $bl->id_header = $out->id_;
                            $bl->status_date = $d->format('Y-m-d');
                            $bl->status_time = $d->format('H:i:s');
                            $bl->save();
                            if ($bl->id_) {
                                return $this->ResponismeTeror(200, "sukses insert data Outbound {$request->waybill_smu}", $bl->id_);
                            }
                        }
                    }
                }
            }
        }
        return $this->ResponismeTeror(404, "Ada yg gagal insert {$request->waybill_smu}", $request->waybill_smu);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}