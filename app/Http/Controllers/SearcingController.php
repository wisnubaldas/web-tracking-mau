<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Warehouse\EksHostawb;
use App\Models\Warehouse\ImpHostAwb;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class SearcingController extends Controller
{
    public function index()
    {
        return view('front.search');
    }
    private function import_to_array($collection)
    {


        $data = [
            'status'  => 'success',
            'message' => 'Tracking hawb ' . $collection->MasterAWB,
            'data'    => [
                'tps'            => 'MAU1',
                'gate_type'      => 'Import',
                'waybill_smu'    => $collection->MasterAWB,
                'hawb'           => $collection->HostAWB,
                'koli'           => $collection->Quantity,
                "netto"          => $collection->Weight,
                "volume"         => $collection->Volume,
                "kindofgood"     => $collection->DescriptionGoods,
                "airline_code"   => $collection->airlinescode,
                "flight_no"      => $collection->FlightNo,
                "origin"         => $collection->Origin,
                "transit"        => null,
                "dest"           => 'CGK',
                "shipper_name"   => $collection->shippername,
                "consignee_name" => $collection->Consigneename,
                "status"         => []
            ]
        ];

        if ($collection->breakdown) {
            $data['data']['status'][] = [
                "status_date" => $collection->breakdown->master->DateEntry,
                "status_time" => $collection->breakdown->master->TimeEntry,
                "code"        => "B1",
                "description" => "Delivery from aircraft to incoming warehouse "
            ];
            $data['data']['status'][] = [
                "status_date" => $collection->breakdown->DateOfBreakdown,
                "status_time" => $collection->breakdown->TimeOfBreakdown,
                "code"        => "B2",
                "description" => "Arrival at Incoming warehouse"
            ];
        }

        if ($collection->do) {
            // ada do tapi ngga ada breakdown
            $br = $this->cek_breakdown(
                $collection->breakdown,
                $collection->do->header->DateOfDeliveryOrder,
                $collection->do->header->TimeOfDeliveryOrder
            );

            if ($br) {
                for ($i = 0; $i < count($br); ++$i) {
                    $data['data']['status'][] = $br[$i];
                }
            }

            $data['data']['status'][] = [
                "status_date" => $collection->do->header->DateOfDeliveryOrder,
                "status_time" => $collection->do->header->TimeOfDeliveryOrder,
                "code"        => "B3",
                "description" => "Storage"
            ];
            $data['data']['status'][] = [
                "status_date" => $collection->do->header->DateOfDeliveryOrder,
                "status_time" => $collection->do->header->TimeOfDeliveryOrder,
                "code"        => "B4",
                "description" => "Custom & quarantine Clearance"
            ];
        }
        if ($collection->pod) {

            // ada pod tapi ngga ada do nya
            $do = $this->cek_do(
                $collection->do,
                $collection->pod->header->DateOfOut,
                $collection->pod->header->TimeOfOut,
            );
            if ($do) {
                for ($i = 0; $i < count($do); ++$i) {
                    $data['data']['status'][] = $do[$i];
                }
            }
            $data['data']['status'][] = [
                "status_date" => $collection->pod->header->DateOfOut,
                "status_time" => $collection->pod->header->TimeOfOut,
                "code"        => "B5",
                "description" => "Received by consignee"
            ];
        }
        return $data;
        // return parent::toArray($request);
    }

    protected function cek_do($do, $date, $time, $minus = -2)
    {
        $time = Carbon::create($time)->add('hour', $minus)->format('H:i:s');
        if (!$do) {
            $data[] = [
                "status_date" => $date,
                "status_time" => $time,
                "code"        => "B3",
                "description" => "Storage"
            ];
            $data[] = [
                "status_date" => $date,
                "status_time" => $time,
                "code"        => "B4",
                "description" => "Custom & quarantine Clearance"
            ];
            return $data;
        }


    }
    protected function cek_breakdown($breakdown, $date, $time, $minus = -1)
    {
        if (!$breakdown) {
            $time = Carbon::create($time)->add('hour', $minus)->format('H:i:s');

            $data[] = [
                "status_date" => $date,
                "status_time" => $time,
                "code"        => "B1",
                "description" => "Delivery from aircraft to incoming warehouse "
            ];
            $data[] = [
                "status_date" => $date,
                "status_time" => $time,
                "code"        => "B2",
                "description" => "Arrival at Incoming warehouse"
            ];
            return $data;
        }
    }
    private function import($request)
    {
        return ImpHostAwb::where('HostAWB', $request->host)
            ->select([
                'MasterAWB',
                'HostAWB',
                'Quantity',
                'Weight',
                'Volume',
                'airlinescode',
                'FlightNo',
                'Origin',
                'DescriptionGoods',
                'shippername',
                'Consigneename'
            ])
            ->with([
                'breakdown' => function ($query) {
                    return $query->select(['BreakdownNumber', 'MasterAWB', 'DateOfBreakdown', 'TimeOfBreakdown'])
                        ->with([
                            'master' => function ($query) {
                                return $query->select(['BreakdownNumber', 'DateEntry', 'TimeEntry']);
                            }
                        ]);
                },
                'do'        => function ($query) {
                    return $query->select(['DONumber', 'MasterAWB', 'HostMAWB', 'BreakdownNumber'])
                        ->with([
                            'header' => function ($query) {
                                return $query->select(['DONumber', 'MasterAWB', 'DateOfDeliveryOrder', 'TimeOfDeliveryOrder']);
                            }
                        ]);
                },
                'pod'       => function ($query) {
                    return $query->select(['TravelNumber', 'HostAWB'])->with([
                        'header' => function ($query) {
                            return $query->select(['TravelNumber', 'DateOfOut', 'TimeOfOut']);
                        }
                    ]);
                }
            ])
            ->first();
    }
    private function export_to_array($collection)
    {
        $data = [
            'status'  => 'success',
            'message' => 'Tracking hawb ' . $collection->HostAWB,
            'data'    => [
                'tps'            => 'MAU1',
                'gate_type'      => 'Ekspor',
                'waybill_smu'    => $collection->MasterAWB,
                'hawb'           => $collection->HostAWB,
                'koli'           => $collection->Quantity,
                "netto"          => $collection->Weight,
                "volume"         => $collection->Volume,
                "kindofgood"     => $collection->descriptiongoods,
                "airline_code"   => $collection->airlinescode,
                "flight_no"      => $collection->FlightNo,
                "origin"         => $collection->master->Origin,
                "transit"        => null,
                "dest"           => $collection->master->Destination,
                "shipper_name"   => $collection->shippername,
                "consignee_name" => $collection->Consigneename,
                "status"         => [
                    [
                        "status_date" => $collection->master->DateEntry,
                        "status_time" => $collection->master->TimeEntry,
                        "code"        => "D1",
                        "description" => "Acceptance confirmation"
                    ],

                ]
            ]
        ];
        if ($collection->approval) {
            $data['data']['status'][] = [
                "status_date" => $collection->approval->DateOfSLI,
                "status_time" => $collection->approval->TimeOFSLI,
                "code"        => "D2",
                "description" => "Weighing Scale"
            ];
            if ($collection->weighing) {
                $data['data']['status'][] = [
                    "status_date" => $collection->weighing->DateEntry,
                    "status_time" => $collection->weighing->TimeEntry,
                    "code"        => "D3",
                    "description" => "Manifesting"
                ];
                if ($collection->buildup) {
                    $data['data']['status'][] = [
                        "status_date" => $collection->buildup->DateEntry,
                        "status_time" => $collection->buildup->TimeEntry,
                        "code"        => "D4",
                        "description" => "Storage Position"
                    ];
                    if ($collection->buildup->master) {
                        $data['data']['status'][] = [
                            "status_date" => $collection->buildup->master->DateEntry,
                            "status_time" => $collection->buildup->master->TimeEntry,
                            "code"        => "D5",
                            "description" => "Build Up process"
                        ];
                        $data['data']['status'][] = [
                            "status_date" => $collection->buildup->master->DateEntry,
                            "status_time" => $collection->buildup->master->EstimateTimeDepature,
                            "code"        => "D6",
                            "description" => "Delivery to Staging Area"
                        ];
                    }
                }
            }
        }

        return $data;
    }
    private function export($request)
    {
        return EksHostawb::select([
            'MasterAWB',
            'HostAWB',
            'Quantity',
            'Weight',
            'Volume',
            'airlinescode',
            'FlightNo',
            'descriptiongoods',
            'shippername',
            'Consigneename',
            'DateOfIn',
            'TimeIn',
            'DateOfOut',
            'TimeOut'
        ])
            ->where('HostAWB', $request->host)
            ->with([
                'master'   => function ($query) {
                    return $query->select([
                        'MasterAWB',
                        'Pieces',
                        'Weight',
                        'Volume',
                        'AirlinesCode',
                        'FlightNo',
                        'Origin',
                        'Destination',
                        'DateOfFlight',
                        'DateEntry',
                        'TimeEntry'
                    ]);
                },
                'approval' => function ($query) {
                    return $query->select(['MasterAWB', 'HostAWB', 'DateOfSLI', 'TimeOFSLI']);
                },
                'weighing' => function ($query) {
                    return $query->select(['MasterAWB', 'HostAWB', 'DateEntry', 'TimeEntry'])->latest();
                },
                'buildup'  => function ($query) {
                    return $query->select(['BuildupNumber', 'MasterAWB', 'DateEntry', 'TimeEntry'])->with([
                        'master' => function ($q) {
                            return $q->select(['BuildupNumber', 'DateEntry', 'TimeEntry', 'EstimateTimeDeparture']);
                        }
                    ]);
                }
            ])
            ->first();
    }
    public function get_international(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'g-recaptcha-response' => 'recaptcha',
            // OR since v4.0.0
            recaptchaFieldName()   => recaptchaRuleName()
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => 'Error capcha'], 500);
        }
        $export = $this->export($request);
        if ($export) {
            return $this->export_to_array($export);
        }
        $import = $this->import($request);
        if ($import) {
            return $this->import_to_array($import);
        }
        return response()->json(['error' => 'Data tidak di temukan'], 500);

    }
    public function get_track(Request $request)
    {
        $validated = $request->validate([
            's' => 'required',
        ]);
        $data = $this->request_to_server($request->s);

        return redirect('/searching')->with('data', $data);
    }
    protected function request_to_server($param)
    {
        $response = Http::withHeaders([
            'X-TOKEN' => 'd51604b0b68c6da5cd04a5ab0b14f1998795e0baf6ae09a07066bae82c0002fe'
        ])->accept('application/json')->post('https://dev-tracking.mitraadira.com/api/host-search', [
                    'host' => $param,
                ]);
        return $response->object();
    }

}