<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImpHostAwb extends Model
{
    use HasFactory;
    protected $connection= 'rdwarehouse_jkt';
    protected $table = 'imp_hostawb';
    protected $primaryKey = 'noid';
    public $timestamps = false;

    public function breakdown()
    {
        return $this->hasOne(ImpBreakdownDetail::class,'MasterAWB','MasterAWB');
    }
    public function do()
    {
        return $this->hasOne(ImpDeliorderdetail::class,'HostMAWB','HostAWB');
    }
    public function pod()
    {
        return $this->hasOne(ImpPoddetail::class,'HostAWB','HostAWB');
    }
}
