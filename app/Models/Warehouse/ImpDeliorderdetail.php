<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImpDeliorderdetail extends Model
{
    use HasFactory;
    protected $connection= 'rdwarehouse_jkt';
    protected $table = 'imp_deliorderdetail';
    protected $primaryKey = 'noid';
    public function header()
    {
        return $this->hasOne(ImpDeliorderheader::class,'DONumber','DONumber');
    }
}
