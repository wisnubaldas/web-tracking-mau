<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Warehouse\ImpPoddetail;

class OutWeighingDetail extends Model
{
    use HasFactory;
    protected $connection = 'rdwarehouse_jkt';
    protected $table = 'out_weighingdetail';
    protected $primaryKey = '_id';

    // public function detail()
    // {
    //     return $this->hasOne(ImpPoddetail::class, 'InvoiceNumber', 'InvoiceNumber');
    // }
}
