<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OutApproval extends Model
{
    use HasFactory;
    protected $connection = 'rdwarehouse_jkt';
    protected $table = 'out_approval';
    protected $primaryKey = '_id';

    public function weighing_header()
    {
        return $this->hasOne(\App\Models\Warehouse\OutWeighingHeader::class, 'MasterAWB', 'MasterAWB');
    }
    public function invoice_detail()
    {
        return $this->hasOne(\App\Models\Warehouse\OutInvoiceDetail::class, 'MasterAWB', 'MasterAWB');
    }
    public function build_up()
    {
        return $this->hasMany(\App\Models\Warehouse\OutBuildupDetail::class, 'MasterAWB', 'MasterAWB');
    }
}
