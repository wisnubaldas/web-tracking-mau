<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Warehouse\OutWeighingDetail;
use App\Models\Warehouse\MstCustomer;

class OutWeighingHeader extends Model
{
    use HasFactory;
    protected $connection = 'rdwarehouse_jkt';
    protected $table = 'out_weighingheader';
    protected $primaryKey = '_id';

    public function detail()
    {
        return $this->hasOne(OutWeighingDetail::class, 'ProofNumber', 'ProofNumber');
    }
    public function customer()
    {
        return $this->hasOne(MstCustomer::class, 'CustomerCode', 'shipperCode');

    }
}
