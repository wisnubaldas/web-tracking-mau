<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EksHostawb extends Model
{
    use HasFactory;
    protected $connection= 'rdwarehouse_jkt';
    protected $table = 'eks_hostawb';
    protected $primaryKey = 'noid';
    // public $incrementing = false;
    // protected $keyType = 'string';
    public $timestamps = false;
    
    public function master()
    {
        return $this->hasOne(EksMasterwaybill::class,'MasterAWB','MasterAWB');
    }
    public function approval()
    {
        return $this->hasOne(EksApproval::class,'HostAWB','HostAWB');
    }
    public function weighing()
    {
        return $this->hasOne(EksWeighingvol::class,'HostAWB','HostAWB');
    }
    public function buildup()
    {
        return $this->hasOne(EksBuildupdetail::class,'MasterAWB','MasterAWB');
    }
}
