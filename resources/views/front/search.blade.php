<!DOCTYPE html>
<html lang="en">

<head>
    {!! htmlScriptTagJsApi() !!}
    @include('front.partial.header')
</head>

<body>
    <div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
        @include('front.partial.navbar')

        <div class="main" id="main"><!-- Main Section-->
            {{-- @include('front.section.first') --}}

            <!-- Feature Image Big -->
            {{-- @include('front.section.big_image') --}}

            <!-- Counter Section -->
            {{-- @include('front.section.counter') --}}


            @if (app('request')->input('q') == 'inter')
                @include('front.section.feature_flex', [
                    'title' => 'International Track & Trace',
                    'action' => '/get-international',
                    'frm_class' => 'form-international',
                ])
                <!-- Counter Section -->
                @include('front.section.counter')
            @elseif (app('request')->input('q') == 'dom')
                @include('front.section.feature_flex', [
                    'title' => 'Domestic Track & Trace',
                    'action' => '/get-domestic',
                    'frm_class' => 'form-domestik',
                ])
                @include('front.section.counter')
            @else
                <!-- Pricing Section -->
                @include('front.section.pricing')
            @endif


            <!-- Client Section -->
            {{-- @include('front.section.client') --}}



            <!-- Footer Section -->
            <div class="footer">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <img src="/template/landingpage/assets/logos/logo.png" alt="Flat Able Logo" />
                        <ul class="footer-menu">
                            <li><a href="https://mitraadira.com/">Home</a></li>
                            <li><a href="https://mitraadira.com/company/">About Us</a></li>
                            <li><a href="https://trackbgd.mitraadira.com/">TRack</a></li>
                            <li><a href="https://mitraadira.com/contact/">Contact</a></li>
                        </ul>
                        <div class="footer-text">
                            <h5>Head Office</h5>
                            <p>Grand Slipi Tower 35th Floor Kav 22-24 Slipi 11480, Jakarta
                                <a href="tel:+622129022500">+62 21 2902 2500</a> / <a href="tel:+622129022600">2600</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Scroll To Top -->

            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>

            <!-- Scroll To Top Ends-->


        </div><!-- Main Section -->
    </div><!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.56/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.56/vfs_fonts.js"></script>
    <script type="text/javascript" src="{{ asset('template/landingpage/assets/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/landingpage/assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/landingpage/assets/js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/landingpage/assets/js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/landingpage/assets/js/menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/landingpage/assets/js/download-pdf-cwp.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/landingpage/assets/js/custom.js') }}"></script>
    {{-- <script type="text/javascript" src="/template/landingpage/assets/js/script.js"></script> --}}

</body>

</html>
