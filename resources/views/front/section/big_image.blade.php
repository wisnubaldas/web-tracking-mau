<div class="feature_huge text-center">
    <div class="container nopadding">
        <div class="col-md-12">
            <img class="img-responsive wow fadeInUp" data-wow-delay="0.1s" src="/template/landingpage/assets/images/big_feature.png" alt="" style="max-width:100%" />
        </div>
        <div class="feature_list">
            <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
                <img src="/template/landingpage/assets/logos/feature_icon.png" alt="Feature" />
                <h1>Tursted Product</h1>
                <p>
                    We increasingly grow our talent and skills in admin dashboard development.
                </p>
            </div>
            <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.4s">
                <img src="/template/landingpage/assets/logos/feature_icon_2.png" alt="Feature" />
                <h1>Online Documentation</h1>
                <p>
                    Documentation helps you in every steps on your entire project.
                </p>
            </div>
            <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
                <img src="/template/landingpage/assets/logos/feature_icon_3.png" alt="Feature" />
                <h1>Free Updates & Support</h1>
                <p>
                    Fast and accurate outline during support. Low turnaround time.
                </p>
            </div>
        </div>
    </div>
</div>
