<div class="client-section">
    <div class="container text-center">
        <div class="clients owl-carousel owl-theme">
            <div class="single">
                <img src="/template/landingpage/assets/logos/logo1.png" alt="" />
            </div>
            <div class="single">
                <img src="/template/landingpage/assets/logos/logo2.png" alt="" />
            </div>
            <div class="single">
                <img src="/template/landingpage/assets/logos/logo3.png" alt="" />
            </div>
            <div class="single">
                <img src="/template/landingpage/assets/logos/logo4.png" alt="" />
            </div>
            <div class="single">
                <img src="/template/landingpage/assets/logos/logo6.png" alt="" />
            </div>
            <div class="single">
                <img src="/template/landingpage/assets/logos/logo7.png" alt="" />
            </div>
        </div>
    </div>
</div>
