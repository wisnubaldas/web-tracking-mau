<div class="hero-section app-hero">
    <div class="container">
        <div class="hero-content app-hero-content text-center">
            <div class="col-md-10 col-md-offset-1 nopadding">
                <h1 class="wow fadeInUp" data-wow-delay="0s">Flat Able Bootstrap 4 Admin Template</h1>
                <p class="wow fadeInUp" data-wow-delay="0.2s">
                    First ever Bootstrap 4 admin template with Flat UI Interface. <br class="hidden-xs"> Its best choice for your any complex project.
                </p>
                <a class="btn btn-info btn-action text-center" data-wow-delay="0.2s" href="http://flatable.phoenixcoded.net">
                    <i class="typcn typcn-world"></i>
                    International Cargo
                </a>
                <a class="btn btn-primary btn-action" data-wow-delay="0.2s" href="https://themeforest.net/item/flat-able-bootstrap-4-admin-template/19842250?ref=phoenixcoded">Buy Now</a>
            </div>

        </div>
    </div>
</div>

{{-- <div class="services-section text-center" id="services"><!-- Services section (small) with icons -->
    <div class="container">
        <div class="col-md-8 col-md-offset-2 nopadding">
            <div class="services-content">
                <h1 class="wow fadeInUp" data-wow-delay="0s">We take care our products for more feature rich</h1>
                <p class="wow fadeInUp" data-wow-delay="0.2s">
                    Flat is one of the finest Admin dashboard template in its category. Premium admin dashboard with high end feature rich possibilities.
                </p>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <div class="services">
                <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="services-icon">
                        <img src="/template/landingpage/assets/logos/icon1.png" height="60" width="60" alt="Service" />
                    </div>
                    <div class="services-description">
                        <h1>Mega feature rich</h1>
                        <p>
                            Flat Able is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="services-icon">
                        <img class="icon-2" src="/template/landingpage/assets/logos/icon2.png" height="60" width="60" alt="Service" />
                    </div>
                    <div class="services-description">
                        <h1>Fast and Robust</h1>
                        <p>
                            We are contantly working on Flat Able and improve its performance too. Your definitely give higher rating to Flat Able for its performance.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="services-icon">
                        <img class="icon-3" src="/template/landingpage/assets/logos/icon3.png" height="60" width="60" alt="Service" />
                    </div>
                    <div class="services-description">
                        <h1>FLAT UI-Interface</h1>
                        <p>
                            Flat able is first ever admin dashboard template which release in Bootstrap 4 framework. Intuitive feature rich design concept and color combination.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

{{-- <div class="flex-features" id="features">
    <div class="container nopadding">
        <div class="flex-split"><!-- Feature section with flex layout -->
            <div class="f-left wow fadeInUp" data-wow-delay="0s">
                <div class="left-content">
                    <img class="img-responsive" src="/template/landingpage/assets/images/feature_1.png" alt="" />
                </div>
            </div>
            <div class="f-right wow fadeInUp" data-wow-delay="0.2s">
                <div class="right-content">
                    <h2>High performance and flexible code</h2>
                    <p>
                        Flat Able is full flexible solution for your entire project development. You can easily maintain all of its module/components.
                    </p>
                    <ul>
                        <li><i class="ion-android-checkbox-outline"></i>Neat n clean code structure.</li>
                        <li><i class="ion-android-checkbox-outline"></i>Flexible module structure</li>
                        <li><i class="ion-android-checkbox-outline"></i>Copy / Paste and Ready to use</li>
                    </ul>
                    <button class="btn btn-primary btn-action btn-fill">Learn More</button>
                </div>
            </div>
        </div>

        <div class="flex-split"><!-- Feature section with flex layout -->
            <div class="f-right">
                <div class="right-content wow fadeInUp" data-wow-delay="0.2s">
                    <h2>Included Software Dependencies</h2>
                    <p>
                        Bower - Grunt - Sass Dependencies for easy project flow management.
                    </p>
                    <ul>
                        <li><i class="ion-android-checkbox-outline"></i>Grunt - No need to update plugins manually</li>
                        <li><i class="ion-android-checkbox-outline"></i>Grunt - Less work you have to performance</li>
                        <li><i class="ion-android-checkbox-outline"></i>Sass - Most Powerful CSS extension language</li>
                    </ul>
                    <button class="btn btn-primary btn-action btn-fill">Learn More</button>
                </div>
            </div>
            <div class="f-left">
                <div class="left-content wow fadeInUp" data-wow-delay="0.3s">
                    <img class="img-responsive" src="/template/landingpage/assets/images/feature_2.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</div> --}}

{{-- <div class="testimonial-section" id="reviews">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <!--  <h1>Destiny is loved by many Startups.</h1>
                    <p class="sub">
                    Listen what your friends are saying about us. Just get the code and sit tight, <br class="hidden-xs"> you'll witness
                    its power and performance in lead conversion.
                    </p>-->
                <div class="testimonials owl-carousel owl-theme">
                    <div class="testimonial-single"><img class="img-circle" src="/template/landingpage/assets/images/testimonial2.jpg" alt="Client Testimonoal" />
                        <div class="testimonial-text wow fadeInUp" data-wow-delay="0.2s">
                            <p>Totally flexible admin template. Easy to use and easy to manage all the elements in entire theme. <br class="hidden-xs"> Great support team behind this product. Low turnaround time with exact
                                support which i needed.
                            </p>
                            <h3>Code Quality</h3>
                            <h3> - amit1134 [Buyer - NZ]</h3>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                        </div>
                    </div>
                    <div class="testimonial-single"><img class="img-circle" src="/template/landingpage/assets/images/testimonial1.jpg" alt="Client Testimonoal" />
                        <div class="testimonial-text">
                            <p>The main reason for the Rating for Able pro admin template is that its is awesome template with tons of ready to use features.<br class="hidden-xs"> - Top quality - Regular updates - PHP version -
                                Clean n Neat code - Saves lots of developing time
                            </p>
                            <h3>Flexibility</h3>
                            <h3>- vishalmg [Buyer -India]</h3>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-ios-star-half"></i>
                        </div>
                    </div>
                    <div class="testimonial-single"><img class="img-circle" src="/template/landingpage/assets/images/testimonial3.jpg" alt="Client Testimonoal" />
                        <div class="testimonial-text">
                            <p>5 stars are for the excellent support, that is brilliant! The design is very cool and the quality of code is excellent. <br class="hidden-xs">Compliments!</p>
                            <h3>Code Quality</h3>
                            <h3>- ab69aho [Buyer -Italy]</h3>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-ios-star-half"></i>
                        </div>
                    </div>
                    <div class="testimonial-single"><img class="img-circle" src="/template/landingpage/assets/images/testimonial2.jpg" alt="Client Testimonoal" />
                        <div class="testimonial-text">
                            <p>The product is high end and high-end specialized assistance in solving problems. <br class="hidden-xs">I would highly recommend.</p>
                            <h3>Customer Support</h3>
                            <h3>- donpavulon [Buyer -US]</h3>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                            <i class="ion ion-star"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
