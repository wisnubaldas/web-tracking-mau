<div class="pricing-section no-color text-center" id="pricing">
    <div class="container">
        <div class="col-md-12 col-sm-12 nopadding">

            <div class="col-sm-6 nopadding">
                <div class="table-left wow fadeInUp" data-wow-delay="0.4s">
                    <div class="pricing-details">
                        <h2>International Cargo</h2>
                        {{-- <span class="typcn typcn-world"></span> --}}
                        <a href="?q=inter">
                            <img class="img img-responsive img-thumbnail" src="/template/landingpage/assets/logos/airplan-1.png" />
                        </a>
                        <p>
                            Cari pengiriman jalur international
                            <br class="hidden-xs"> dengan nomer HAWB.
                        </p>

                        <a class="btn btn-primary btn-action btn-fill" href="?q=inter">Tracking Now</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 nopadding">
                <div class="table-right wow fadeInUp" data-wow-delay="0.6s">
                    <div class="pricing-details">
                        <h2>Domestic Cargo</h2>
                        <a href="?q=dom">
                            <img class="img img-responsive img-thumbnail" src="/template/landingpage/assets/logos/airplan-2.png" />
                        </a>
                        {{-- <span class="typcn typcn-plane span-1"></span> --}}
                        <p>
                            Cari status pengiriman
                            <br class="hidden-xs"> pada jalur domestik
                        </p>
                        <a class="btn btn-primary btn-action btn-fill" href="?q=dom">Tracking Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .img-thumbnail {
        max-height: 250px;
    }
    .table-left .pricing-details span,
    .table-right .pricing-details span {
        display: inline-block;
        font-size: 200px;
        font-weight: 600;
        color: #402857;
        margin-bottom: 20px;
        background: #A7CF9F;
        background: linear-gradient(to bottom right, #A7CF9F 14%, #114D0D 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;



    }

    .table-left .pricing-details span,
    .table-right .pricing-details span .span-1 {
        background: #CF82CF;
        background: repeating-linear-gradient(to bottom left, #CF82CF 0%, #8A1751 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;

    }
</style>
