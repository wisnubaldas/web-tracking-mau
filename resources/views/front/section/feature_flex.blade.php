<div class="features-section"><!-- Feature section with flex layout -->
    <div class="f-left">
        <div class="left-content wow fadeInLeft" data-wow-delay="0s">
            <h2 class="wow fadeInLeft" data-wow-delay="0.1s">{{ $title }}</h2>
            <p class="wow fadeInLeft" data-wow-delay="0.2s">
                Track my package has never been that easy with this global parcel tracking tool allowing you to track parcels from any warehouse. A powerful universal parcel tracking system that will provide you with shipment info by
                tracking number lookup from any international courier such as MAU Tracking.
            </p>

        </div>
        <div class="left-content wow fadeInLeft" data-wow-delay="1s">
            <form id="frm-track" style="text-align:left" class="subscribe-form {{ $frm_class }} wow zoomIn" action="{{ $action }}" method="post" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off"
                novalidate>
                @csrf
                <input class="mail" type="text" name="host" placeholder="AWB Number" autocomplete="off" />
                <input class="submit-button" type="submit" />
                <hr />
                {!! htmlFormSnippet(['callback' => 'rechapchaCallback']) !!}
            </form>
        </div>
    </div>
    <div class="f-right" style="padding-top:100px;">

    </div>
</div>
