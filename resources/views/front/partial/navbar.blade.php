        <div class="container">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand page-scroll" href="#main"><img src="/template/landingpage/assets/logos/logo.png" alt="Flat Able Logo" /></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a class="page-scroll" href="https://mitraadira.com/">Home</a></li>
                            <li><a class="page-scroll" href="https://mitraadira.com/company/">About us</a></li>
                            <li><a class="page-scroll" href="https://mitraadira.com/our-expertise/">Our Expertise</a></li>
                            <li><a class="page-scroll" href="https://mitraadira.com/network/">Our Network</a></li>
                            <li><a class="page-scroll" href="https://mitraadira.com/facilities/">Facilities</a></li>
                            <li><a class="page-scroll" href="https://trackbgd.mitraadira.com/">Track</a></li>
                            <li><a class="page-scroll" href="https://mitraadira.com/contact/">Contacts</a></li>
                            <li><a class="page-scroll" href="{{ route('login') }}">Login</a></li>
                        </ul>
                    </div>
                </div>
            </nav><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
