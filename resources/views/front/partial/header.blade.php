<meta charset="utf-8">
<title>Mitra Adira Utama - Track & Tracking </title>
<!-- Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Landing page template for creative dashboard">
<meta name="keywords" content="Landing page template">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Favicon icon -->
<link rel="icon" href="{{ asset('template/landingpage/assets/logos/favicon.ico') }}" type="image/png" sizes="16x16">
<!-- Bootstrap -->
<link href="{{ asset('template/landingpage/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
<!-- typicon icon -->
<link rel="stylesheet" type="text/css" href="{{ asset('template/assets/icon/typicons-icons/css/typicons.min.css') }}">
<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('template/landingpage/assets/css/animate.css') }}">
<!-- Owl Carousel -->
<link rel="stylesheet" href="{{ asset('template/landingpage/assets/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('template/landingpage/assets/css/owl.theme.css') }}">
<!-- Magnific Popup -->
<link rel="stylesheet" href="{{ asset('template/landingpage/assets/css/magnific-popup.css') }}">
<!-- Full Page Animation -->
<link rel="stylesheet" href="{{ asset('template/landingpage/assets/css/animsition.min.css') }}">
<!-- Ionic Icons -->
<link rel="stylesheet" href="{{ asset('template/landingpage/assets/css/ionicons.min.css') }}">
<!-- Main Style css -->
<link href="{{ asset('template/landingpage/assets/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
