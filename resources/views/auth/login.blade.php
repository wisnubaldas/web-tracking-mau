@extends('back.index')
@section('content')
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->
                    <div class="login-card card-block auth-body">
                        <form class="md-float-material" action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="text-center">
                                <img src="{{ asset('template/assets/images/auth/logo.png') }}" alt="logo.png">
                            </div>
                            <div class="auth-box">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-left txt-primary">Sign In</h3>
                                    </div>
                                </div>
                                <hr />
                                <div class="input-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    <span class="md-line"></span>
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="input-group">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    <span class="md-line"></span>
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="row m-t-25 text-left">
                                    <div class="col-sm-7 col-xs-12">
                                        <div class="checkbox-fade fade-in-primary">
                                            <label>
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                <span class="text-inverse">Remember me</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-xs-12 forgot-phone text-right">
                                        <a href="#" class="text-right f-w-600 text-inverse"> Forgot Your Password?</a>
                                    </div>
                                </div>
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Sign in</button>
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-10">
                                        <p class="text-inverse text-left m-b-0">Thank you and enjoy our website.</p>
                                        <p class="text-inverse text-left"><b>BGD-MAU 2024</b></p>
                                    </div>
                                    <div class="col-md-2">
                                        <img src="{{ asset('template/assets/images/auth/Logo-small-bottom.png') }}" alt="small-logo.png">
                                    </div>
                                </div>

                            </div>
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- Authentication card end -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 9]>
                                                        <div class="ie-warning">
                                                            <h1>Warning!!</h1>
                                                            <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                                                            <div class="iew-container">
                                                                <ul class="iew-download">
                                                                    <li>
                                                                        <a href="http://www.google.com/chrome/">
                                                                            <img src="assets/images/browser/chrome.png" alt="Chrome">
                                                                            <div>Chrome</div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                                                                            <img src="assets/images/browser/firefox.png" alt="Firefox">
                                                                            <div>Firefox</div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="http://www.opera.com">
                                                                            <img src="assets/images/browser/opera.png" alt="Opera">
                                                                            <div>Opera</div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="https://www.apple.com/safari/">
                                                                            <img src="assets/images/browser/safari.png" alt="Safari">
                                                                            <div>Safari</div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                                                            <img src="assets/images/browser/ie.png" alt="">
                                                                            <div>IE (9 & above)</div>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <p>Sorry for the inconvenience!</p>
                                                        </div>
                                                    <![endif]-->
    <!-- Warning Section Ends -->
@endsection
