{{-- @dump($cwp) --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.56/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.56/vfs_fonts.js"></script>
<script>
    let cekObj = function(obj) {
        if (obj === null) {
            return 'Tidak ada data'
        } else {
            return obj
        }
    }
    let downloadPdf = function(data) {
        // let cwpDate = cekObj(data.weighing_header)
        // let buildUpDate = cekObj(data.invoice_detail)
        // let baggage = function(x) {
        //     let y = []
        //     for (var key in x.build_up) {
        //         let obj = x.build_up[key];
        //         y.push([{
        //             text: moment(obj.created_at).format('YYYY-MM-DD hh:mm:s'),
        //             colSpan: 2
        //         }])
        //     }
        //     return y
        // }
        var docDefinition = {
            content: [{

                    columns: [{
                        image: 'data:image/jpeg;base64,{{ base64_encode(file_get_contents(url('/logo/mau-logo.png'))) }}',
                        width: 150,
                        margin: [0, 0, 0, 0]
                    }, {
                        image: 'data:image/jpeg;base64,{{ base64_encode(file_get_contents(url('/logo/bgd.jpg'))) }}',
                        width: 100,
                        margin: [250, 0, 0, 0]
                    }]
                },
                {
                    text: "Cargo Weighing Proof\n\n\n\n",
                    style: "header",
                    alignment: 'center',
                    decoration: "underline"
                },
                {
                    columns: [{
                            width: 90,
                            text: 'CWP',
                            style: 'statusCWP'
                        },
                        {
                            width: '*',
                            text: ' : ' + data.ProofNumber,
                            style: 'statusCWP'
                        },
                    ]
                },
                {
                    columns: [{
                            width: 90,
                            text: 'Company Agent',
                            style: 'statusCWP'
                        },
                        {
                            width: '*',
                            text: ' : ' + data.CompanyName,
                            style: 'statusCWP'
                        },
                    ]
                },
                {
                    columns: [{
                            width: 90,
                            text: 'Flight No / Date',
                            style: 'statusCWP'
                        },
                        {
                            width: '*',
                            text: ' : ' + data.FlightNumber + " / " + data.DateOfFlight,
                            style: 'statusCWP'
                        },
                    ]
                },
                {
                    columns: [{
                            width: 90,
                            text: 'Destination',
                            style: 'statusCWP'
                        },
                        {
                            width: '*',
                            text: ' : ' + data.Destination,
                            style: 'statusCWP'
                        },
                    ]
                },
                {
                    columns: [{
                            width: 90,
                            text: 'AWB',
                            style: 'statusCWP'
                        },
                        {
                            width: '*',
                            text: ' : ' + data.MasterAWB,
                            style: 'statusCWP'
                        },
                    ]
                },
                {
                    style: 'tableExample',
                    table: {
                        body: [
                            ['AWB-H/AWB-SMU', 'PCS', 'DIMENSION', 'ACTUAL WEIGHT', 'VOLUME WEIGHT', 'CHARGEABLE WEIGHT', 'NATURE OF GOOD'],
                            [
                                data.HostAWB,
                                data.Pieces,
                                `${data.LongCargo} x ${data.WidthCargo} x ${data.HighCargo}`,
                                data.NettoWeight,
                                data.VolumeCargo,
                                (parseFloat(data.NettoWeight) < parseFloat(data.VolumeCargo)) ? data.VolumeCargo : data.CAW,
                                data.KindOfNature,
                            ],
                        ]
                    },
                    layout: {
                        fillColor: function(rowIndex, node, columnIndex) {
                            return (rowIndex % 2 === 0) ? '#CCCCCC' : null;
                        }
                    }
                },
            ],
            styles: {
                header: {
                    fontSize: 13,
                    bold: true,
                    alignment: 'justify',
                    color: '#0d4f38'
                },
                tableExample: {
                    margin: [0, 5, 0, 15],
                    fontSize: 10,
                    alignment: 'center',
                },
                tableHeader: {
                    bold: true,
                    fontSize: 10,
                    color: 'black'
                },
                statusCWP: {
                    fontSize: 10
                }
            }

        };
        // console.log(docDefinition)
        const pdfDocGenerator = pdfMake.createPdf(docDefinition);
        pdfDocGenerator.getDataUrl((dataUrl) => {
            console.log(dataUrl)
        });
        pdfDocGenerator.open({}, window);
    }


    let data = @json($cwp);
    downloadPdf(data)
    // let r = "{{ route('cwp') }}";
    // window.location.replace(r);
</script>
