@extends('back.parsial.main-content')
@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-header-title">
                            <h4>CWP</h4>
                        </div>
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="index.html">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Pages</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">CWP</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="page-body ">
                        <div class="row">
                            <!-- Autofill table start -->
                            <div class="card col-md-12">
                                <div class="card-header">
                                    <h5>Auto Fill</h5>
                                    <div class="card-header-right">
                                        <i class="icofont icofont-rounded-down"></i>
                                        <i class="icofont icofont-refresh"></i>
                                        <i class="icofont icofont-close-circled"></i>
                                    </div>

                                </div>
                                <div class="card-block">
                                    {{-- <div class="col-sm-12">
                                        <form method="POST" id="search-form" role="form">
                                            @csrf
                                            <div class="input-group input-group-button">
                                                <input name="ProofNumber" id="ProofNumber" type="text" class="form-control" placeholder="Right Button">
                                                <span class="input-group-addon" id="basic-addon10">
                                                    <button type="submit" class="btn btn-primary">Search</button>
                                                </span>
                                            </div>
                                        </form>

                                    </div> --}}
                                    <div class="dt-responsive table-responsive">
                                        <table id="autofill" class="table table-striped table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th>CWP Number</th>
                                                    <th>HAWB</th>
                                                    <th>Customer Code</th>
                                                    <th>Date Of Flight</th>
                                                    <th>Time</th>
                                                    <th>#</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- Autofill table end -->
                        </div>
                    </div>
                </div>

                {{-- <div id="styleSelector"></div> --}}
            </div>
        </div>
    </div>
@endsection
@push('css')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/assets/pages/data-table/extensions/autofill/css/autoFill.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/assets/pages/data-table/extensions/autofill/css/select.dataTables.min.css') }}">
@endpush
@push('js')
    <!-- data-table js -->
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('template/assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('template/assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('template/assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('template/assets/pages/data-table/extensions/autofill/js/dataTables.autoFill.min.js') }}"></script>
    <script src="{{ asset('template/assets/pages/data-table/extensions/autofill/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var oTable = $('#autofill').DataTable({
                autoFill: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('cwp-data') }}",
                order: [
                    [3, 'desc']
                ],
                columns: [{
                        data: 'ProofNumber'
                    },
                    {
                        data: 'MasterAWB'
                    },
                    {
                        data: 'shipperCode'
                    },
                    {
                        data: 'DateOfFlight'
                    },
                    {
                        data: 'TimeOfEntry'
                    },
                    {
                        data: 'pdf'
                    }
                ]
            });
            // $('#search-form').on('submit', function(e) {
            //     oTable.draw();
            //     e.preventDefault();
            // });
        });
    </script>
@endpush
